let subscribeTopic = process.env.SUBSCRIBE_TOPIC || "#"; 
let fileNameStub = process.env.FILE_NAME_STUB || './logs/' + subscribeTopic;
let mqttBrokerHost = process.env.BROKER_HOST || "localhost";
let mqttBrokerPort = +process.env.BROKER_PORT || 1883;
let mqttBrokerProtocol = process.env.BROKER_PROTOCOL || "tcp";
let mqttBrokerUserName = process.env.BROKER_USERNAME;
let mqttBrokerPassword = process.env.BROKER_PASSWORD;


import  {connect, MqttClient,IClientOptions, Packet} from 'mqtt';
import Logger from './logger';
let logger = new Logger(fileNameStub);
console.log("Connecting to:", `${mqttBrokerProtocol}://${mqttBrokerHost}:${mqttBrokerPort}`);
let mqttClient:MqttClient = connect( {
    host:mqttBrokerHost,
    port:mqttBrokerPort,
    protocol:mqttBrokerProtocol,
    username:mqttBrokerUserName,
    password:mqttBrokerPassword,
    reconnectPeriod:500,
    connectTimeout:500,
    resubscribe:true,
    keepAlive:1

});

mqttClient.on("error", ()=>{
    logger.LogStatus("error");
})

mqttClient.on("connect", (err)=>{
    logger.LogStatus("connect");
})

mqttClient.on("reconnect", (err)=>{
    logger.LogStatus("reconnect");
})

mqttClient.on("reconnect", (err)=>{
    logger.LogStatus("reconnect");
})
mqttClient.on("end", (err)=>{
    logger.LogStatus("end");
})

mqttClient.on("close", (err)=>{
    logger.LogStatus("close");
})


mqttClient.on("offline", (err)=>{
    logger.LogStatus("offline");
})

mqttClient.on("message", (topic: string, payload: Buffer, packet: any)=>{
    logger.LogMqttMessage(topic, payload, packet.retain, packet.qos)
});

console.log("subcribing to topic:", subscribeTopic);
mqttClient.subscribe(subscribeTopic);
