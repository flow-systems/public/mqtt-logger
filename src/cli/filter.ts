let filename = process.argv[2]
let topic = process.argv[3] || "#";
let value = process.argv[4];

function topicMatches(filter, topic) 
{
    return topic.match(filter.replace("\\+", "[^/]+").replace("#", ".+"))
}

function valueMatches(filterValue, actualValue)
{
    try
    {
        if( actualValue=== "" )
        {
            return filterValue === "";
        }
        else if(filterValue!=="")
        {
            return JSON.parse(actualValue)===JSON.parse(filterValue)
        }     
        return false;
    }
    catch(err)
    {
        return false;
    }
}

var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream(filename)
  });


  lineReader.on('line', function (line) {
    let obj = JSON.parse(line);
    if( topicMatches(topic, obj.topic))
    {
        if( value === undefined || valueMatches(value, obj.payload ) )
        {
            console.log(line);
        }
    }
  });

  lineReader.onClose
