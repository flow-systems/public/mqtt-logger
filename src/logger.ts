import * as fs from 'fs';
import * as path from 'path';
import * as mkdirp from 'mkdirp';

export default class Logger
{
    private filestub;
    constructor(filestub:string)
    {
        if( filestub.substr(filestub.length-1)==='#')
        {
            filestub = filestub.substr(0, filestub.length-1);
        }
        this.filestub = filestub;

        // According to https://stackoverflow.com/questions/31645738/how-to-create-full-path-with-nodes-fs-mkdirsync
        // the following code shoud work with current versions of node but I can't get it to compile
        /*
        fs.mkdirSync(
            path.dirname(this.destFileName), 
            {recursive:true}
        );
        */
       // So instead using mkdirp
       mkdirp((path.dirname(this.destFileName)));
    }

    get destFileName()
    {
        return path.resolve(process.cwd(), 
         this.filestub + Logger.dateToString(new Date())+ '.log');
    }

    static dateToString(date:Date):string
    {
        let retval = +date.getUTCFullYear()+'-'+ 
        ("0" + (date.getUTCMonth()+1)).slice(-2) + '-' +
        ("0" + date.getUTCDate()).slice(-2) ;
        return retval;
    }
    

    createLogMessage(topic:string, buf:Buffer, retain:boolean, qos:number):object
    {
        let payload = buf.toString();
        if( payload ) 
        {
            try
            {
                payload = JSON.parse(buf.toString());
            }
            catch(error)
            {
                console.log("Non JSON string!");
            }
        }
        return {topic:topic, payload:payload, retain:retain, qos:qos};
    
    }

    private writeJson(obj:any)
    {
        obj.time=new Date();
        let msg = JSON.stringify(obj);
        console.log("logging:", msg);
        fs.appendFile(this.destFileName,
            msg + '\n',(err)=>{
                if(err)
                {
                    console.log("Error writing to file", this.destFileName, err.message )
                }
            });
    }
    
    public LogStatus(status:string)
    {
        this.writeJson({status:status});
    }

    public LogMqttMessage(topic:string, buf:Buffer, retain:boolean, qos:number)
    {
        let msg = this.createLogMessage(topic,buf,retain,qos);
        this.writeJson(msg);
    }
}