import * as mocha from 'mocha';
import * as chai from 'chai';
import {expect} from 'chai';

import Logger from '../../src/logger';

describe("Logger", ()=>{
  describe("dateToString()", ()=>{
    it("should create a string in the format {YYYY-MM-DD", ()=>{
      let date = new Date();
      date.setUTCFullYear(2019);
      date.setUTCMonth(5); // 0 indexed
      date.setUTCDate(23);
      date.setUTCMinutes(34); // irrelevant
      date.setUTCHours(3); // irrelevant
      date.setUTCSeconds(32); // irrelevant

      expect(Logger.dateToString(date)).to.equal("2019-06-23");
    });
  })

});