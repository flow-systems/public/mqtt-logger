# This relies on `npm build` having first been run from outside the container
FROM node:carbon

USER root

COPY ./package*.json ./
COPY ./dist ./dist

COPY ./node_modules ./node_modules

# Now remove everything we don't need - prune doesn't work if you have npm packages linked
# on your dev machine but its faillure won't affect its use in testing, hence the ; exit 0
RUN npm prune --production; exit 0

# This is only required for debugging - can't it be exposed via docker-compose if necessary?
# EXPOSE 3002

CMD ["node","dist/index.js"]